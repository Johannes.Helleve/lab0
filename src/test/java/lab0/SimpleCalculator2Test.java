package lab0;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleCalculator2Test {
    @Test
    void twoPlusTwo(){
        SimpleCalculator2 calc = new SimpleCalculator2();
        assertEquals(4, calc.add(2,2));
        //assertNotEquals(); not equals
    }
}