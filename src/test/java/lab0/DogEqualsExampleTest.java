package lab0;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DogEqualsExampleTest {

    @Test
    void checkTwoSimilarDogs(){
        DogEqualsExample dog1 = new DogEqualsExample("Labrador", "Oscar");
        DogEqualsExample dog2 = new DogEqualsExample("Labrador", "Oscar");
        assertEquals(dog1,dog2);
    }
    @Test
    void checkTwoUnSimilarDogs(){
        DogEqualsExample dog1 = new DogEqualsExample("Labrador", "Oscar");
        DogEqualsExample dog2 = new DogEqualsExample("Labrador", "Mitty");
        assertNotEquals(dog1,dog2);
    }

}