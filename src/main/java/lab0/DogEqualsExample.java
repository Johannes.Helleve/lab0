package lab0;

import java.util.Objects;

public class DogEqualsExample {
    private String breed;
    private String name;

    public DogEqualsExample(String breed, String name){
        this.breed = breed;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DogEqualsExample that = (DogEqualsExample) o;
        return Objects.equals(breed, that.breed) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(breed, name);
    }
}
